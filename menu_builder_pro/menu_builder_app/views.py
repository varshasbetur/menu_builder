from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets
from menu_builder_app.models import (
                                Modifier,
                                MenuSection,
                                Item
                                )
from rest_framework.generics import (
                        CreateAPIView,
                        DestroyAPIView,
                        ListAPIView,
                        RetrieveAPIView,
                        RetrieveUpdateAPIView,
                        RetrieveUpdateDestroyAPIView
                        )

from menu_builder_app.serializers import (
                        MenuSectionSerializer,
                        ItemsSerializer,
                        ModifiersSerializer,
                        ItemslistSerializer,
                        MenulistSerializer
                        )

#------Menusection CRUD-------------------
# CRUD Using Generics


class MenuSectionAPIView(CreateAPIView):
    """
    :Created-by:varsha
    :purpose:create a MenuSection instance
    :input: ["section_name","description"]
    :models: MenuSection
    :Output: Newly created MenuSection instance
    """

    serializer_class = MenuSectionSerializer


class MenuSectionsDeleteAPIView(DestroyAPIView):
    """
        :Created-by:varsha
        :purpose:deletes a MenuSection instance
        :input: [pk]
        :models: MenuSection
        :Output: 200 status(delets MenuSection instance(soft delete))
    """
    def delete(self, request, *args, **kwargs):
        try:
            queryset = MenuSection.objects.get(id=kwargs['pk'])
            queryset.is_delete = True
            queryset.save()
        except :
            return Response(
                'This Menu sections does not exist',
                status=status.HTTP_404_NOT_FOUND
            )
        return Response("Success", status=status.HTTP_200_OK)


class MenuSectionRetrieveAPIView(RetrieveAPIView):
    """
        :Created-by:varsha
        :purpose:fetch a perticular MenuSection instance
        :input: ["pk"]
        :models: MenuSection
        :Output: MenuSection instance
    """

    queryset = MenuSection.objects.filter(is_delete=False)
    serializer_class = MenuSectionSerializer


class AllMenuSectionRetrieveAPIView(ListAPIView):
    """
        :Created-by:varsha
        :purpose:fetch all MenuSection instances
        :input: []
        :models: MenuSection
        :Output: All MenuSection instances
    """

    queryset = MenuSection.objects.filter(is_delete=False)
    serializer_class = MenuSectionSerializer


class MenuSectionsUpdateAPIView(RetrieveUpdateAPIView):
    """
        :Created-by:varsha
        :purpose:Updates a MenuSection instance
        :input: ["pk",data which needs to update]
        :models: MenuSection
        :Output: Updated MenuSection instance
    """
    serializer_class = MenuSectionSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", True)
        try:
            instance = MenuSection.objects.get(id=kwargs.get('pk'), is_delete=False)
        except :
            return Response(
                ' Section not Found',
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = MenuSectionSerializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

# CRUD Using Viewset
class MenuSectionsViewSet(viewsets.ModelViewSet):
    serializer_class = MenuSectionSerializer
    queryset = MenuSection.objects.all()

#------Item CRUD-------------------

# CRUD Using Viewset


class ItemsViewSet(viewsets.ModelViewSet):
    serializer_class = ItemsSerializer
    queryset = Item.objects.all()


# CRUD Using Generics
class ItemAPIView(CreateAPIView):
    """
        :Created-by:varsha
        :purpose:create a Item instance
        :input: ["item_name","description", "price", "section", "modifier"]
        :models: Item
        :Output: Newly created Item instance
    """

    serializer_class = ItemsSerializer


class AllItemsRetrieveAPIView(ListAPIView):
    """
        :Created-by:varsha
        :purpose:fetch all Item instances
        :input: []
        :models: Item
        :Output: All Item instances
    """
    queryset = Item.objects.filter(is_delete=False)
    serializer_class = ItemsSerializer


class ItemsDeleteAPIView(DestroyAPIView):
    """
        :Created-by:varsha
        :purpose:deletes a Item instance
        :input: [pk]
        :models: Item
        :Output: 200 status(deletes Item instance(soft delete))
    """

    def delete(self, request, *args, **kwargs):
        try:
            queryset = Item.objects.get(id=kwargs['pk'])
            queryset.is_delete = True
            queryset.save()
        except:
            return Response(
                'This Item does not exist',
                status=status.HTTP_404_NOT_FOUND
            )
        return Response("Success", status=status.HTTP_200_OK)


class ItemsRetrieveAPIView(RetrieveAPIView):
    """
        :Created-by:varsha
        :purpose:fetch a perticular Item instance
        :input: ["pk"]
        :models: Item
        :Output: Item instance
    """

    queryset = Item.objects.filter(is_delete=False)
    serializer_class = ItemsSerializer


class ItemUpdateAPIView(RetrieveUpdateAPIView):
    """
        :Created-by:varsha
        :purpose:Updates Item instance
        :input: ["pk",data which needs to update]
        :models: Item
        :Output: Updated Item instance
    """

    # queryset = Item.objects.all()
    serializer_class = ItemsSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", True)
        try:
            instance = Item.objects.get(id=kwargs.get('pk'), is_delete=False)
        except:
            return Response(
                'Item not Found',
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = ItemsSerializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

#------Modifier CRUD-------------------


# CRUD Using Viewset

class ModifierViewSet(viewsets.ModelViewSet):

    serializer_class = ModifiersSerializer
    queryset = Modifier.objects.all()


# CRUD Using Generics
class ModifiersAPIView(CreateAPIView):
    """
        :Created-by:varsha
        :purpose:create a Modifier instance
        :input: ["description"]
        :models: Modifier
        :Output: Newly created Modifier instance
    """

    serializer_class = ModifiersSerializer


class ModifierDeleteAPIView(DestroyAPIView):
    """
        :Created-by:varsha
        :purpose:deletes a Modifier instance
        :input: [pk]
        :models: Modifier
        :Output: 200 status(deletes a Modifier instance(soft delete))
    """

    def delete(self, request, *args, **kwargs):
        try:
            queryset = Modifier.objects.get(id=kwargs['pk'])
            queryset.is_delete = True
            queryset.save()
        except:
            return Response(
                'This Modifier does not exist',
                status=status.HTTP_404_NOT_FOUND
            )
        return Response("Success", status=status.HTTP_200_OK)


class ModifierRetrieveAPIView(RetrieveAPIView):
    """
        :Created-by:varsha
        :purpose:fetch a perticular Modifier instance
        :input: ["pk"]
        :models: Modifier
        :Output: Modifier instance
    """
    queryset = Modifier.objects.filter(is_delete=False)
    serializer_class = ModifiersSerializer


class ModifierUpdateAPIView(RetrieveUpdateAPIView):
    """
        :Created-by:varsha
        :purpose:Updates Modifier instance
        :input: ["pk", data which needs to update]
        :models: Modifier
        :Output: Updated Modifier instance
    """
    serializer_class = ModifiersSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", True)
        try:
            instance = Modifier.objects.get(id=kwargs.get('pk'), is_delete=False)
        except:
            return Response(
                'Modifier not Found',
                status=status.HTTP_404_NOT_FOUND
            )
        serializer = ModifiersSerializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class AllmodifierRetrieveAPIView(ListAPIView):
    """
        :Created-by:varsha
        :purpose:fetch all Modifier instances
        :input: []
        :models: Modifier
        :Output: All Modifier instances
    """
    queryset = Modifier.objects.filter(is_delete=False)
    serializer_class = ModifiersSerializer


# fetch menusections related data
class FetchMenuViewSet(viewsets.ModelViewSet):
    """
        :Created-by:varsha
        :purpose:fetch all data related to Menusections
        :input: []
        :models: MenuSection
        :Output: All Menusections related data
    """
    serializer_class = MenulistSerializer
    queryset = MenuSection.objects.all()


# add modifiers to items
class ModifiertoItemsUpdateAPIView(RetrieveUpdateAPIView):
    """
        :Created-by:varsha
        :purpose:Updates extra modifiers to items instance
        :input: ["pk", "modifiers"]
        :models: Item
        :Output: adds Modifier instance to items
    """
    serializer_class = ItemsSerializer

    def update(self, request, *args, **kwargs):
        # partial = kwargs.pop("partial", True)
        try:
            instance = Item.objects.get(id=kwargs.get('pk'), is_delete=False)
        except:
            return Response(
                'Item not Found',
                status=status.HTTP_404_NOT_FOUND
            )
        existing_modifiers = instance.modifier.all()
        modifiers_list = Modifier.objects.filter(id__in=request.data["modifier"])
        for i in modifiers_list:
            if i not in existing_modifiers:
                instance.modifier.add(i)

        item_data = Item.objects.get(id=kwargs.get('pk'))
        serializer = ItemsSerializer(item_data)
        return Response(serializer.data)




