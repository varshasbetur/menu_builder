from rest_framework import serializers
from menu_builder_app.models import (
                                Modifier,
                                MenuSection,
                                Item
                                )
from django.db.models import F

class MenuSectionSerializer(serializers.ModelSerializer):
    """
    created by:
    """

    class Meta:
        model = MenuSection
        fields = "__all__"


class ModifiersSerializer(serializers.ModelSerializer):
    """
    created by:
    """
    class Meta:
        model = Modifier
        fields = "__all__"


class ItemsSerializer(serializers.ModelSerializer):
    """
    created by:
    """
    section = MenuSectionSerializer

    class Meta:
        model = Item
        fields = "__all__"


class ItemslistSerializer(serializers.ModelSerializer):
    """
    created by:
    """
    modifier = ModifiersSerializer()

    class Meta:
        model = Item
        fields = ["id", "item_name", "modifier"]


class MenulistSerializer(serializers.ModelSerializer):
    """
    created by:
    """
    items = serializers.SerializerMethodField(read_only=True)
    title = serializers.SerializerMethodField()

    class Meta:
        model = MenuSection
        fields = ["id", "title", "items"]

    def get_items(self, obj):
        final_list = []
        try:
            item_instances = Item.objects.filter(section_id=obj.id)
            for i in item_instances:
                sub_dict = dict()
                modifier_instances = i.modifier.all().annotate(title=F('description')).values("id", "title")
                sub_dict['id'] = i.id
                sub_dict['title'] = i.item_name
                sub_dict['modifiers'] = modifier_instances
                final_list.append(sub_dict)
            return final_list

        except:
            return None

    def get_title(self, obj):
        return obj.section_name
