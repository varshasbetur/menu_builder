from django.apps import AppConfig


class MenuBuilderAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'menu_builder_app'
