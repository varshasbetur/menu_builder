from django.db import IntegrityError
from django.test import TestCase
from ..models import Modifier, MenuSection, Item


class ModifiersTest(TestCase):
    def setUp(self):
        self.modifiers = Modifier()
        self.modifiers.description = "Extra cheese"
        self.modifiers.save()

        self.menu_secton = MenuSection()
        self.menu_secton.section_name = "Snacks Special"
        self.menu_secton.description = "Snacks"
        self.menu_secton.save()

        self.item = Item()
        self.item.item_name = "peeza"
        self.item.discription = "snacks item"
        self.item.modifiers = [1]
        self.item.section = self.menu_secton
        self.item.save()


    #-----------------menu_section-------------------------
    def test_get_menu_sections(self):
        menu_secton_instance = MenuSection.objects.get(id=1)
        self.assertEquals(menu_secton_instance, self.menu_secton)

    def test_menu_section_on_save(self):
        self.assertEquals(self.menu_secton.section_name, "Snacks Special")

    def test_menu_section_description_validation_error(self):
        self.menu_secton.description = None
        with self.assertRaises(IntegrityError):
            self.menu_secton.save()

    def test_menu_section_name_validation_error(self):
        self.menu_secton.section_name = None
        with self.assertRaises(IntegrityError):
            self.menu_secton.save()

    # -----------------modifiers-------------------------
    def test_get_modifiers(self):
        modifier_instance = Modifier.objects.get(id=1)
        self.assertEquals(modifier_instance, self.modifiers)

    def test_modifiers_on_save(self):
        self.assertEquals(self.modifiers.description, "Extra cheese")

    def test_modifier_validation_error(self):
        self.modifiers.description = None
        with self.assertRaises(IntegrityError):
            self.modifiers.save()

    # -----------------item-------------------------
    def test_get_item(self):
        item_instance = Item.objects.get(id=1)
        self.assertEquals(item_instance, self.item)

    def test_item_on_save(self):
        self.assertEquals(self.item.item_name, "peeza")

    def test_items_validation_error(self):
        self.item.section = None
        self.item.description = None
        with self.assertRaises(IntegrityError):
            self.item.save()

    def test_item_name_validation_error(self):
        self.item.item_name = None
        with self.assertRaises(IntegrityError):
            self.item.save()