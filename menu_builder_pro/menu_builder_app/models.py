from django.db import models


class MenuSection(models.Model):
    section_name = models.CharField(max_length=120)
    description = models.CharField(max_length=120)
    is_delete = models.BooleanField(default=False)

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return self.section_name


class Modifier(models.Model):
    description = models.CharField(max_length=120)
    is_delete = models.BooleanField(default=False)


class Item(models.Model):
    item_name = models.CharField(max_length=120)
    description = models.CharField(max_length=120)
    price = models.FloatField(
        blank=True, null=True,
        default=0.0
    )
    section = models.ForeignKey(MenuSection, on_delete=models.CASCADE)
    modifier = models.ManyToManyField(Modifier)
    is_delete = models.BooleanField(default=False)

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return self.item_name




