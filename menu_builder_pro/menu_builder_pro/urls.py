"""menu_builder_pro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path as url
from django.urls import path, include
from menu_builder_app.views import *

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'menusections', MenuSectionsViewSet)
router.register(r'modifiers', ModifierViewSet, basename='Modifiers')
router.register(r'items', ItemsViewSet, basename='Item')
router.register(r'menu_list', FetchMenuViewSet)

urlpatterns = [
    url(r"^create-menu_section/$", MenuSectionAPIView.as_view()),
    path('delete-menu_section/<pk>', MenuSectionsDeleteAPIView.as_view()),
    path('update-menu_section/<pk>/', MenuSectionsUpdateAPIView.as_view()),
    path('fetch-menu_section/<pk>/', MenuSectionRetrieveAPIView.as_view()),
    path('fetch-all-menu_section/', AllMenuSectionRetrieveAPIView.as_view()),

    url(r"^create-item/$", ItemAPIView.as_view()),
    path('delete-item/<pk>', ItemsDeleteAPIView.as_view()),
    path('update-item/<pk>/', ItemUpdateAPIView.as_view()),
    path('fetch-item/<pk>/', ItemsRetrieveAPIView.as_view()),
    path('fetch-all-items/', AllItemsRetrieveAPIView.as_view()),


    url(r"^create-modifier/$", ModifiersAPIView.as_view()),
    path('delete-modifier//<pk>', ModifierDeleteAPIView.as_view()),
    path('update-modifier/<pk>/', ModifierUpdateAPIView.as_view()),
    path('fetch-modifier/<pk>/', ModifierRetrieveAPIView.as_view()),
    path('fetch-all-modifier/', AllmodifierRetrieveAPIView.as_view()),

    path('add-modifier-to-items/<pk>/', ModifiertoItemsUpdateAPIView.as_view())
]
urlpatterns += router.urls