# menu_builder

Menu App which discribes, detailed menu structure, which consists of menu section, Item and Modifiers.
This app is build in "python django", The API are built to perform CRUD for menu section, Item and Modifiers,
and to fetch detailed menu list and API for adding modifiers to a particular item


```
git clone https://gitlab.com/varshasbetur/menu_builder.git
create virtual env and install dependences from requirement.txt file
cd menu_builder_pro
sorce env/bin/activate
python manage.py runserver
```
